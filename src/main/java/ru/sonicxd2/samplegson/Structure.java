/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ru.sonicxd2.samplegson;

import lombok.Getter;

/**
 *
 * @author Ярослав
 */
public class Structure {
    @Getter AnotherStructure[] array;
    @Getter AnotherStructure someStructure;

    public Structure(AnotherStructure[] array, AnotherStructure someStructure) {
        this.array = array;
        this.someStructure = someStructure;
    }
    
    
}
