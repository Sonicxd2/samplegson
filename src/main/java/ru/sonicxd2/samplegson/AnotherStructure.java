/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ru.sonicxd2.samplegson;

import lombok.Getter;

/**
 *
 * @author Ярослав
 */
public class AnotherStructure {
    @Getter int a;

    public AnotherStructure(int a) {
        this.a = a;
    }
    
}
