/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ru.sonicxd2.samplegson;

import com.google.gson.Gson;
import java.util.Scanner;

/**
 * @see "This is very simple sample of use Gson"
 * @author Ярослав
 */
public class Main {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        
        //Init Gson
        Gson g = new Gson();
        
        Structure struct = g.fromJson(sc.nextLine(), Structure.class); //Get custom object from String
        
        String json = g.toJson(struct); //Serialised Structure using Gson
        
        System.out.println(json);
    }
}
